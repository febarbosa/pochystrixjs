# README #

Para subir todos os serviços, aplicações e dashboard executar na raiz do projeto no terminal:
node index.js

Serviços (portas): 3001,3002,3003

Aplicações (portas): 3004, 3005

Dashboard (portas): 8000

Os serviços e aplicações estão configurados para apresentarem latência e indisponibilidade.

Para iniciar o monitoramento do funcionamento adicionar o stream de log de cada aplicação ao dashboard:

http://localhost:3004/api/hystrix.stream

http://localhost:3005/api/hystrix.stream

![2F8cCD.jpg](https://bitbucket.org/repo/akyoqKz/images/998300357-2F8cCD.jpg)